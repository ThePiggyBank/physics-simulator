#include <iostream>
#include <fstream>
#include <random>
#include <kraken/math/matrix.hpp>
#include <functional>
#include <SFML/Graphics.hpp>
#include <chrono>
#include "gamescreen.hpp"

class Timer
{
public:
    using Seconds = double;
    void start()
    {
        m_start = std::chrono::high_resolution_clock::now();
    }
    void stop()
    {
        m_stop = std::chrono::high_resolution_clock::now();
    }
    Seconds duration()
    {
        std::chrono::duration<Seconds> dur(m_stop - m_start);
        return dur.count();
    }
public:
    using Timepoint = decltype(std::chrono::high_resolution_clock::now());
    Timepoint m_start;
    Timepoint m_stop;
};

int main()
{
    {
    bool isClicked = false;
    float timeSinceLastShot = 0;
    std::string choice = "menu";
    std::ifstream config("/home/rasmus/fysiksimulator/src/content/config/config.txt");
    if (!config.is_open())
    {
        throw std::runtime_error("Config.txt not found.");
    }

    std::random_device rdev;
    random(0, 0, rdev());
    sf::RenderWindow window(sf::VideoMode(1920, 1080), "Breakout vindue");
    window.setVerticalSyncEnabled(true);
    Timer timer;
    Gamescreen breakout(window, config);
    timer.start();
    config.close();
    std::ofstream configs("/home/rasmus/fysiksimulator/src/content/config/config.txt", std::ios::app);
    while (window.isOpen())
    {
        timer.stop();
        float dt = timer.duration();
        timer.start();
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                if (event.type == sf::Event::Closed)
                {
                    window.close();
                }
            }
            if ((event.type == sf::Event::MouseButtonPressed)
                && (event.key.code == sf::Mouse::Left))
            {
                    isClicked = true;
            }
        }
        if (choice == "editor")
        {
            window.clear();
            breakout.editorMode(window, configs);
            window.display();
            continue;
        }
        else if (choice == "game")
        {
            window.clear();
            if (timeSinceLastShot > 1)
            {
                breakout.handleInput(window);
            }
            else
            {
                timeSinceLastShot += dt;
            }
            breakout.update(dt);
            breakout.draw(window);
            window.display();
        }
        else
        {
            window.clear();
            choice = breakout.mainMenu(window, isClicked);
            if (isClicked && choice == "empty")
            {
                isClicked = false;
            }
            window.display();
        }
    }
    }
}
