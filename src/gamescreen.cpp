#include "gamescreen.hpp"
#include <iostream>
#include <sstream>
#include <random>
#include <chrono>

struct CollisionInfo
{
    float distance;
    vec2 point;
    int brickNumber;
};

class Timer
{
public:
    using Seconds = double;
    void start()
    {
        m_start = std::chrono::high_resolution_clock::now();
    }
    void stop()
    {
        m_stop = std::chrono::high_resolution_clock::now();
    }
    Seconds duration()
    {
        std::chrono::duration<Seconds> dur(m_stop - m_start);
        return dur.count();
    }
public:
    using Timepoint = decltype(std::chrono::high_resolution_clock::now());
    Timepoint m_start;
    Timepoint m_stop;
};

double random(double low, double high, unsigned seed)
{
    static std::mt19937 gen(seed);
    std::uniform_real_distribution<double> dist(low, high);
    return dist(gen);
}

vec2 pointLineParameters(vec2 p, vec2 c, vec2 v, float r)
{
    float max = std::numeric_limits<float>::max();
    vec2 q = p-c;/*
    if (squaredLength(q) < r*r)
    {
        throw std::logic_error("Point inside ball.");
    }*/
    if (dot(q, v) > 0)
    {
        return {std::numeric_limits<float>::max(),std::numeric_limits<float>::max()};
    }
    float nv = dot(v, v);
    float tBase = -dot(q,v)/nv;
    float subfac = dot(q,v)*dot(q, v)-nv*(dot(q,q)-r*r);
    float tExtra = sqrt(subfac)/nv;
    float t0 = tBase - tExtra;
    float t1 = tBase + tExtra;
    if (t0 <= 0)
    {
        t0 = max;
    }
    if (t1 <= 0)
    {
        t1 = max;
    }
    return {t0, t1};
}

vec2 lineLineParameters(vec2 p0, vec2 p1, vec2 q0, vec2 q1)
{
    //auto denom = (p1.x - p0.x)*(q0.y - q1.y) - (q0.x - q1.x) * (p1.y - p0.y);

    mat2 m(q0.y - q1.y, q1.x - q0.x,
           p0.y - p1.y, p1.x - p0.x);
    auto denom = det(m);
    if (std::abs(denom) <= 1e-6f)
    {
        denom = 1e-6f;
        //throw std::logic_error("Not implemented.");
        // TODO: Handle degenerate case.
    }
    return (1/denom) * m * (q0-p0);
}

vec2 lineCollision(vec2 speed, vec2 ballPosition, vec2 q0, vec2 q1)
{
    vec2 p0 = ballPosition;
    vec2 p1 = p0 + speed;
    vec2 ts = lineLineParameters(p0, p1, q0, q1);
    return ts;
}

std::pair<float, vec2> ballLineCollision(float ballRadius, vec2 speed, vec2 ballPosition, vec2 q0, vec2 q1)
{
    vec2 q = normalise(perp(q1-q0));
    if (dot(q, speed) < 0)
    {
        return {std::numeric_limits<float>::max(), {0,0}};
    }
    vec2 col0 = lineCollision(speed, ballPosition, q0-q*ballRadius, q1-q*ballRadius);
    vec2 col1 = lineCollision(speed, ballPosition, q0+q*ballRadius, q1+q*ballRadius);
    float t = std::numeric_limits<float>::max();
    if (col0[1] >= 0 && col0[1] <= 1 && col0[0] >= 0)
    {
        t = std::min(t, col0[0]);
    }
    if (col1[1] >= 0 && col1[1] <= 1 && col1[0] >= 0)
    {
        t = std::min(t, col1[0]);
    }
    return {t, q1-q0};
}

CollisionInfo ballCollision(vec2 ballPosition, vec2 speed, float ballRadius, std::vector<Brick> objects, float width, float height)
{
    CollisionInfo closestDistance = {std::numeric_limits<float>::max(), {0, 0}, -1};
    auto tempVar = ballLineCollision(ballRadius, speed, ballPosition, {width, 0}, {0, 0});
    CollisionInfo t = {tempVar.first, tempVar.second, -1};

    if (closestDistance.distance > t.distance)
    {
        closestDistance = t;
    }
    tempVar = ballLineCollision(ballRadius, speed, ballPosition, {width, height}, {width, 0});
    t = {tempVar.first, tempVar.second, -1};

    if (closestDistance.distance > t.distance)
    {
        closestDistance = t;
    }
    tempVar = ballLineCollision(ballRadius, speed, ballPosition, {0, 0}, {0, height});
    t = {tempVar.first, tempVar.second, -1};

    if (closestDistance.distance > t.distance)
    {
        closestDistance = t;
    }

    for (int i = 0; i < objects.size(); ++i)
    {
        for (int j = 0; j < objects[i].size(); ++j)
        {
            vec2 p0 = objects[i][j];
            vec2 p1 = objects[i][(j+1) % objects[i].size()];

            vec2 temp = pointLineParameters(ballPosition, p0, speed, ballRadius);
            float nt = std::min(temp.x, temp.y);
            t = {nt, perp(ballPosition + nt*speed - p0), i};

         //   std::pair<float, vec2> t1 = {pointLineParameters(ballPosition, p1, speed, ballRadius), p1-p0};
            tempVar = ballLineCollision(ballRadius, speed, ballPosition, p0, p1);
            CollisionInfo t2 = {tempVar.first, tempVar.second, i};

            if (closestDistance.distance > t2.distance)
            {
                closestDistance = t2;
            }
            if (closestDistance.distance > t.distance)
            {
                closestDistance = t;
            }
        }
    }

    return closestDistance;
}

CollisionInfo paddleCollision(float ballRadius, float paddleHeight, vec2 speed, vec2 ballPosition, vec2 p0, vec2 p1, vec2 q0, vec2 q1)
{
    CollisionInfo closestDistance = {std::numeric_limits<float>::max(), {0, 0}, -1};

    auto tempVar = ballLineCollision(ballRadius, speed, ballPosition, p0, p1);
    CollisionInfo line = {tempVar.first, tempVar.second, 0};


    vec2 temp = pointLineParameters(ballPosition, q0, speed, ballRadius+paddleHeight/2);
    float nt = std::min(temp.x, temp.y);
    CollisionInfo rightBall = {nt, perp(ballPosition + nt*speed - q0), 0};

    temp = pointLineParameters(ballPosition, q1, speed, ballRadius+paddleHeight/2);
    nt = std::min(temp.x, temp.y);
    CollisionInfo leftBall  = {nt, perp(ballPosition + nt*speed - q1), 0};

    if (closestDistance.distance > line.distance)
    {
        closestDistance = line;
    }
    if (closestDistance.distance > rightBall.distance)
    {
        closestDistance = rightBall;
    }
    if (closestDistance.distance > leftBall.distance)
    {
        closestDistance = leftBall;
    }
    return closestDistance;
}

vec2 reflect(vec2 v, vec2 l)
{
    return 2.f * dot(v, l)/dot(l,l)*l - v;
}

float Gamescreen::getPaddleWidth()
{
    return m_paddleDim.x;
}

float Gamescreen::getBallSpeed()
{
    for (int i = 0; i < m_ball.size(); ++i)
    {
        vec2 m_ballVel = m_ball[i].vel;
        return length(m_ballVel);
    }
}

vec2 Gamescreen::getScreenSize()
{
    return m_windowDim;
}

void Gamescreen::setPaddleWidth(float width)
{
    std::cerr << "Shrunk... \n";
    if (width < m_windowDim.x/19)
    {
        m_paddleDim.x = m_windowDim.x/19;
    }
    else if (width > m_windowDim.x/3)
    {
        m_paddleDim.x = m_windowDim.x/3;
    }
    else
    {
        m_paddleDim.x = width;
    }
}

void Gamescreen::enableGuns()
{
    m_enableGuns = true;
}

void Gamescreen::enableBallGrabber()
{
    m_grabBall = true;
}

void Gamescreen::setBallSpeed(float speed)
{
    for (int i = 0; i < m_ball.size(); ++i)
    {
        vec2 m_ballVel = m_ball[i].vel;
        m_ballVel = speed * normalise(m_ballVel);
        m_ball[i].vel = m_ballVel;
    }
}

void Gamescreen::enablePassthrough()
{
    m_passthrough = true;
}

void Gamescreen::modifyLife(int change)
{
    m_lives += change;
}

void Gamescreen::modifyScore(int change)
{
    m_score += change;
}

void Gamescreen::enableFireball()
{
    m_noDurability = true;
}

void Gamescreen::octuplicateBall()
{
    float xOffset;
    float yOffset;

    int ballsize = m_ball.size();
    for (int i = 0; i < ballsize; ++i)
    {
        if (m_ball.size() > 84)
        {
            return;
        }
        if (m_ball[i].type == "laser")
        {
            continue;
        }
        for (int j = 0; j < 8; ++j)
        {
            Ball ball;
            ball.vel = {m_ball[i].vel.x, m_ball[i].vel.y};
            ball.ballOffset = m_ball[i].ballOffset;
            ball.launchball = m_ball[i].launchball;
            ball.ballRadius = m_ball[i].ballRadius;
            if (m_ball[i].pos.x < 400)
            {
                yOffset = m_ball[i].pos.y/2;
            }
            else
            {
                xOffset = 400;
            }

            if (m_ball[i].pos.y < 400)
            {
                yOffset = m_ball[i].pos.y/2;
            }
            else
            {
                yOffset = 400;
            }


            ball.pos = {m_ball[i].pos.x + random(0, -xOffset), m_ball[i].pos.y + random(0, -yOffset)};
            ball.texture = "ball";
            m_ball.push_back(ball);
        }
    }
}

void Gamescreen::duplicateBall()
{
    int ballsize = m_ball.size();
    for (int i = 0; i < ballsize; ++i)
    {
        if (m_ball.size() > 84)
        {
            return;
        }
        if (m_ball[i].type == "laser")
        {
            continue;
        }
        Ball ball;
        ball.vel = {m_ball[i].vel.x * -1, m_ball[i].vel.y * -1};
        ball.ballOffset = m_ball[i].ballOffset;
        ball.launchball = m_ball[i].launchball;
        ball.ballRadius = m_ball[i].ballRadius;
        ball.pos = m_ball[i].pos;
        ball.texture = "ball";
        m_ball.push_back(ball);
    }
}

void Gamescreen::performPowerupCollision(float dt)
{
    for (int i = 0; i < m_powerup.size(); ++i)
    {
        vec2 acc = {m_windowDim.x/50.f, m_windowDim.y/3.f};
        vec2 fwd = m_powerup[i].pos;// - 4*dt*m_powerup[i].vel + dt*dt/2 * acc;
        if (fwd.x+m_powerup[i].dims.x >= m_paddlePos.x-m_paddleDim.x/2-m_paddleDim.y/4
            && fwd.x <= m_paddlePos.x + m_paddleDim.x/2 + m_paddleDim.y/4
            && fwd.y > m_windowDim.y-m_paddleDim.y-m_powerup[i].dims.y
            && fwd.y < m_windowDim.y)
        //if (fwd.y > m_windowDim.y-m_paddleDim.y-m_powerup.dims.y)
        {
            auto currentPowerup = m_powerup[i];
            m_powerup.erase(m_powerup.begin() + i);
            currentPowerup.action(*this);
            std::cerr << "You picked up the powerup!";
            m_score += 1000;
        }
        fwd = m_powerup[i].pos;
        if (fwd.x+m_powerup[i].dims.x >= m_windowDim.x)
        {
            std::cerr << "Collision";
            m_powerup[i].vel = reflect(m_powerup[i].vel, perp(vec2{-1, 0}));
        }
        if (fwd.x <= 0)
        {
            std::cerr << "Collision";
            m_powerup[i].vel = reflect(m_powerup[i].vel, perp(vec2{-1, 0}));
        }
        if (fwd.y <= 0)
        {
            std::cerr << "Collision";
            m_powerup[i].vel = reflect(m_powerup[i].vel, perp(vec2{0, -1}));
        }
        if (m_powerup[i].pos.y > m_windowDim.y)
        {
            m_powerup.erase(m_powerup.begin() + i);
            std::cerr << "You didn't pick up the powerup";
        }
        else
        {
            m_powerup[i].pos += dt*m_powerup[i].vel + dt*dt/2 * acc;
            m_powerup[i].vel += dt*acc;
        }
    }
}

sf::Vector2f toSFML(vec2 v)
{
    return {v.x, v.y};
}

Gamescreen::Gamescreen(sf::Window& window, std::istream& config)
{
    reset(window, config);
}

sf::Texture& Gamescreen::getTexture(std::string name) const
{
    auto elem = m_textures.find(name);
    if (elem == m_textures.end())
    {
        auto& texture = (m_textures[name] = {});
        texture.loadFromFile("/home/rasmus/fysiksimulator/src/textures/" + name + ".png");
        return texture;
    }
    return elem->second;
}

Gamescreen::Powerup Gamescreen::generateRandomPowerup(int i)
{
        vec2 m_ballVel = m_ball[i].vel;
        vec2 m_ballPos = m_ball[i].pos;
        vec2 m_ballOffset = m_ball[i].ballOffset;
        bool m_launchBall = m_ball[i].launchball;
        Powerup powerup;
        powerup.pos =  {m_ballPos.x + random(-50, 50), m_ballPos.y + random(-50, 50)};
        powerup.dims = {1.5*m_windowDim.x/40, 1.5*m_windowDim.x/40};
        if (powerup.pos.y < 0)
        {
            powerup.pos.y = 0;
        }
        if (powerup.pos.x < 0)
        {
            powerup.pos.x = 0;
        }
        if (powerup.pos.x+powerup.dims.x > m_windowDim.x)
        {
            powerup.pos.x = m_windowDim.x - powerup.dims.x;
        }
        if (powerup.pos.y > m_windowDim.y-m_windowDim.y/3)
        {
            powerup.pos.y = m_windowDim.y-m_windowDim.y/3;
        }
        powerup.vel = normalise(vec2{random(-1, 1), random(-1, 1)}) * (float)random(m_windowDim.x/4, m_windowDim.x/3);

        auto rnum = random(0, 1);
        if (rnum < 0.05)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.enablePassthrough();
            };
            powerup.texture = "thrublock";
        }
        else if (rnum < 0.1)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.modifyLife(1);
            };
            powerup.texture = "plusone";
        }
        else if (rnum < 0.2)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.setPaddleWidth(screen.getPaddleWidth()-screen.getScreenSize().x/39);
            };
            powerup.texture = "paddlesmaller";
        }
        else if (rnum < 0.35)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.setPaddleWidth(screen.getPaddleWidth()+screen.getScreenSize().x/39);
            };
            powerup.texture = "paddlelarger";
        }
        else if (rnum < 0.4)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.enableGuns();
            };
            powerup.texture = "guns";
        }
        else if (rnum < 0.45)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.enableFireball();
            };
            powerup.texture = "fireball";
        }
        else if (rnum < 0.5)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.setBallSpeed(screen.getBallSpeed() * 1.2);
            };
            powerup.texture = "fastball";
        }
        else if (rnum < 0.55)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.setBallSpeed(screen.getBallSpeed() / 1.2);
            };
            powerup.texture = "slowball";
        }
        else if (rnum < 0.6)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.modifyLife(-1);
            };
            powerup.texture = "minusone";
        }
        else if (rnum < 0.7)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.modifyScore(-11000);
            };
            powerup.texture = "minusscore";
        }
        else if (rnum < 0.75)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.enableBallGrabber();
            };
            powerup.texture = "ballgrabber";
        }
        else if (rnum < 0.8)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.duplicateBall();
            };
            powerup.texture = "ballduplicator";
        }
        else if (rnum < 0.85)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.octuplicateBall();
            };
            powerup.texture = "octuplicate";
        }
        else if (rnum < 1)
        {
            powerup.action = [](Gamescreen& screen)
            {
                screen.modifyScore(1000);
            };
            powerup.texture = "plusscore";
        }


    /*
    powerup.action = [](Gamescreen& screen)
    {
        screen.enablePassthrough();

        this->m_ballRadius = std::abs(std::sin(m_ballRadius)) * 20.f+2.f;;;;;;
        this->m_paddleDim.x = std::abs(std::sin(m_paddleDim.x)) * 250.f+100.f;;;;;;
        this->m_powerup.push_back(this->generateRandomPowerup());
        this->m_score += 109000000;
    };
    */
    return powerup;
}

void Gamescreen::shootWithGuns()
{
    float speed = m_windowDim.x/4;
    vec2 dir{0.0001, -1};

    Ball laserLeft;
    laserLeft.vel = speed * normalise(dir);
    laserLeft.ballOffset = {0, 0};
    laserLeft.launchball = true;
    laserLeft.ballRadius = m_windowDim.x/200;
    laserLeft.pos = {m_paddlePos.x-m_paddleDim.x/2+m_windowDim.y/110/2, m_paddlePos.y - m_paddleDim.y/2 - m_windowDim.x/110};
    laserLeft.type = "laser";
    laserLeft.texture = "laser";
    m_ball.push_back(laserLeft);

    Ball laserRight;
    laserRight.vel = speed * normalise(dir);
    laserRight.ballOffset = {0, 0};
    laserRight.launchball = true;
    laserRight.ballRadius = m_windowDim.x/200;
    laserRight.pos = {m_paddlePos.x+m_paddleDim.x/2-m_windowDim.y/110/2, m_paddlePos.y - m_paddleDim.y/2 - m_windowDim.x/110};
    laserRight.type = "laser";
    laserRight.texture = "laser";
    m_ball.push_back(laserRight);
}

void Gamescreen::handlePaddleInput(float dt)
{
    m_paddlePos.x -= m_paddleLeft * m_paddleVel*dt;
    m_paddlePos.x += m_paddleRight * m_paddleVel*dt;

    for (int i = 0; i < m_ball.size(); ++i)
    {
        if (m_ball[i].type == "laser")
        {
            continue;
        }
        vec2 m_ballVel = m_ball[i].vel;
        vec2 m_ballPos = m_ball[i].pos;
        vec2 m_ballOffset = m_ball[i].ballOffset;
        bool m_launchBall = m_ball[i].launchball;
        float m_ballRadius =  m_ball[i].ballRadius;


        if (m_lives <= 0)
        {
            onDeath(dt);
        }

        if (m_paddlePos.x - m_paddleDim.x/2 - m_paddleLeft * m_paddleVel*dt - m_paddleDim.y/2 < 0)
        {
           m_paddlePos.x = m_paddleDim.x/2 + m_paddleDim.y/2;
        }

        if (m_paddlePos.x + m_paddleDim.x/2 + m_paddleRight * m_paddleVel*dt + m_paddleDim.y/2 > m_windowDim.x)
        {
           m_paddlePos.x = m_windowDim.x - m_paddleDim.x/2 - m_paddleDim.y/2;
        }

        if (m_ballPos.y - m_ballRadius > m_windowDim.y)
        {
            int standardBallAmount = m_ball.size();
            for (int i = 0; i < m_ball.size(); ++i)
            {
                if (m_ball[i].type == "laser")
                {
                    standardBallAmount -= 1;
                }
            }

            if (standardBallAmount > 1)
            {
                m_ball.erase(m_ball.begin() + i);
                --i;
                continue;
            }
            else
            {
                m_launchBall = false;
                m_ballOffset.x = 0;
                --m_lives;
            }
        }

        if (!m_launchBall)
        {
            vec2 dir = {1, -1};
            if (m_ballOffset.x < 0)
            {
                dir = {-1, -1};
            }

            float speed = m_windowDim.x/4;
            m_ballVel = speed * normalise(dir);
        }


        if (m_grabBall && m_launchBall)
        {
            m_ballOffset.x = m_ballPos.x-m_paddlePos.x;
        }
        if (m_paddlePos.x + m_ballOffset.x + m_ballLeft * m_paddleVel*dt < m_paddlePos.x + m_paddleDim.x/2 - m_paddleDim.y/4)
        {
            m_ballOffset.x += m_ballLeft * m_paddleVel*dt;
        }
        if (m_paddlePos.x - m_ballOffset.x - m_ballRight * m_paddleVel*dt < m_paddlePos.x + m_paddleDim.x/2 - m_paddleDim.y/3)
        {
            m_ballOffset.x -= m_ballRight * m_paddleVel*dt;
        }
        else if (m_ballPos.x < m_paddlePos.x - m_paddleDim.x/2)
        {
            m_ballOffset.x = m_paddleDim.y/4 - m_paddleDim.x/2;
        }
        //If ball lands on the edges of the paddle:
        if (m_ballPos.x > m_paddlePos.x + m_paddleDim.x/2 - m_paddleDim.y/4)
        {
            m_ballOffset.x = m_paddleDim.x/2 - m_paddleDim.y/3;
        }

        if (!m_launchBall)
        {
            m_ballPos = vec2 {m_paddlePos.x, m_paddlePos.y - m_paddleDim.y/2 - m_ballRadius} + m_ballOffset;
        }

        m_ball[i].vel = m_ballVel;
        m_ball[i].pos = m_ballPos;
        m_ball[i].ballOffset = m_ballOffset;
        m_ball[i].launchball = m_launchBall;
        m_ball[i].ballRadius = m_ballRadius;
    }
}

void Gamescreen::readLevel(std::istream& config)
{
    std::string buf;
    m_currLevel = 0;
    while(std::getline(config, buf))
    {
        std::stringstream reader(buf);
        float x, y, width, height;
        int durability, value;
        std::string type, texture;
        reader >> type;
        if (type == "Level")
        {
            reader >> m_currLevel;
            m_currLevel -= 1;
            while (m_levels.size() <= m_currLevel)
            {
                m_levels.push_back({});
            }
            m_levels[m_currLevel].clear();
            continue;
        }
        if (type != "brick") continue;
        if (m_levels.size() <= m_currLevel)
        {
            throw std::runtime_error("No current level (malformed input).");
        }
        //if ()

        reader >> x >> y >> width >> height >> durability >> value >> texture;

        width = m_windowDim.x*0.041667;
        height = m_windowDim.y*0.030325;
        x = m_windowDim.x * x;
        y = m_windowDim.y * y;

        Brick brick;
        brick.points.push_back(vec2{x, y });
        brick.points.push_back(vec2{x + width, y});
        brick.points.push_back(vec2{x + width, y + height});
        brick.points.push_back(vec2{x, y+ height});
        brick.durability = durability;
        brick.value = value;
        brick.texture = texture;
        m_levels[m_currLevel].push_back(brick);
    }
}

void Gamescreen::loadLevel()
{
    m_ball.clear();
    m_powerup.clear();
    m_paddleDim = {m_windowDim.x/16*3, m_windowDim.y/34*1.3};
    m_paddlePos = {m_windowDim.x/2, m_windowDim.y - m_paddleDim.y/2};
    m_paddleVel = m_windowDim.x/4;
    float speed = m_windowDim.x/4;
    vec2 dir{1, -1};

    Ball ball;
    ball.vel = speed * normalise(dir);
    ball.ballOffset = {0, 0};
    ball.launchball = false;
    ball.ballRadius = m_windowDim.x/110;
    ball.pos = {m_paddlePos.x, m_paddlePos.y - m_paddleDim.y/2 - m_windowDim.x/110};
    ball.texture = "ball";
    m_ball.push_back(ball);

    m_paddleLeft = false;
    m_paddleRight = false;
    m_dead = false;
    m_passthrough = false;
    m_grabBall = false;
    m_enableGuns = false;
    m_noDurability = false;
}

void Gamescreen::reset(sf::Window& window, std::istream& config)
{
    m_texture = "standard";
    m_currElem = 0;
    m_windowDim = vec2{window.getSize().x, window.getSize().y};
    float speed = window.getSize().x/4;
    vec2 dir{1, -1};

    m_editorBrickPos = {0, 0};


    readLevel(config);
    m_level = 0;
    m_bricks = m_levels[m_level];
    m_paddleDim = {m_windowDim.x/16*3, m_windowDim.y/34*1.3};
    m_paddlePos = {m_windowDim.x/2, m_windowDim.y - m_paddleDim.y/2};
    m_paddleVel = m_windowDim.x/4;
    m_mousePos = sf::Mouse::getPosition(window).x;

    for (int i = 0; i < 1; ++i)
    {
        Ball ball;
        ball.vel = speed * normalise(dir);
        ball.ballOffset = {0, 0};
        ball.launchball = false;
        ball.ballRadius = m_windowDim.x/110;
        ball.pos = {m_paddlePos.x, m_paddlePos.y - m_paddleDim.y/2 - m_windowDim.x/110};
        ball.texture = "ball";
        m_ball.push_back(ball);
    }

    m_popTime = 0;
    m_score = 0;
    m_lives = 3;
    m_paddleLeft = false;
    m_paddleRight = false;
    m_dead = false;
    m_passthrough = false;
    m_grabBall = false;
    m_enableGuns = false;
    m_pauseGame = false;
    m_wait = false;
    m_noDurability = false;
    m_timeSinceLastShot = 0;
    generateBricks();
}

void Gamescreen::handleInput(sf::Window& window)
{
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::P)
        || sf::Keyboard::isKeyPressed(sf::Keyboard::E))
    {
        if(!m_wait)
        {
            if(m_pauseGame)
            {
                m_pauseGame = false;
            }
            else
            {
                m_pauseGame = true;
            }
            m_wait = true;
        }
    }
    else
    {
        m_wait = false;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::C))
    {
         m_enableCheats = true;
    }
    if (m_enableCheats)
    {
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num0))
        {
            m_enableGuns = true;
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
        {
            m_lives = 300;
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
        {
            m_grabBall = true;
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
        {
            m_noDurability = true;
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num4))
        {
            m_enableGuns = true;
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num5))
        {
            m_score += 10000000000;
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num6))
        {
            octuplicateBall();
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num7))
        {
            duplicateBall();
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num8))
        {
            if (m_allowSkipLevel)
            {
                loadLevel();
                if (m_level+1 == m_levels.size())
                {
                    std::cerr << "You did it you beat the levels";
                }
                else
                {
                    m_level += 1;
                    m_bricks = m_levels[m_level];
                }
            }
            m_allowSkipLevel = false;
        }
        else
        {
            m_allowSkipLevel = true;
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num9))
        {
            m_passthrough = true;
        }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)
        || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
    {
        window.close();
    }

    if (m_pauseGame)
    {
        return;
    }

    if (m_timeSinceLastShot > 0.3)
    {
        if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
            || sf::Mouse::isButtonPressed(sf::Mouse::Right))
            && !m_dead && m_enableGuns)
        {
                shootWithGuns();
                m_timeSinceLastShot = 0;
        }
    }
    for (int i = 0; i < m_ball.size(); ++i)
    {
        vec2 m_ballVel = m_ball[i].vel;
        vec2 m_ballPos = m_ball[i].pos;
        vec2 m_ballOffset = m_ball[i].ballOffset;
        bool m_launchBall = m_ball[i].launchball;
        float m_ballRadius =  m_ball[i].ballRadius;
        bool usingMouse = false;
        float newPosition = sf::Mouse::getPosition(window).x;
        if (m_mousePos != newPosition)
        {
            usingMouse = true;
            m_mousePos = newPosition;
        }

        if (usingMouse && !m_dead)
        {
            if (m_mousePos < m_paddleDim.x/2 - m_paddleDim.y/2)
            {
                m_paddlePos.x = m_paddleDim.x/2 + m_paddleDim.y/2;
            }
            else if (m_mousePos > m_windowDim.x-m_paddleDim.x/2-m_paddleDim.y/2)
            {
                m_paddlePos.x =  m_windowDim.x-m_paddleDim.x/2-m_paddleDim.y/2;
            }
            else
            {
                m_paddlePos.x = m_mousePos;
            }
        }
        else if (!m_dead)
        {
            m_paddleLeft = sf::Keyboard::isKeyPressed(sf::Keyboard::Left)
                           || sf::Keyboard::isKeyPressed(sf::Keyboard::A);
            m_paddleRight = sf::Keyboard::isKeyPressed(sf::Keyboard::Right)
                            || sf::Keyboard::isKeyPressed(sf::Keyboard::D);
        }
        else if (m_dead)
        {
            m_paddleLeft = false;
            m_paddleRight = false;
        }

        if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
            || sf::Keyboard::isKeyPressed(sf::Keyboard::Space)
            || sf::Mouse::isButtonPressed(sf::Mouse::Left))
            && !m_dead)
        {
            m_launchBall = true;
        }

        if (!m_dead)
        {
            m_ballLeft = sf::Keyboard::isKeyPressed(sf::Keyboard::L);
            m_ballRight = sf::Keyboard::isKeyPressed(sf::Keyboard::J);
        }
        m_ball[i].vel = m_ballVel;
        m_ball[i].pos = m_ballPos;
        m_ball[i].ballOffset = m_ballOffset;
        m_ball[i].launchball = m_launchBall;
        m_ball[i].ballRadius = m_ballRadius;
    }
}



void Gamescreen::update(float dt)
{
    std::cerr << m_level << "\n";
    if (m_bricks.size() == 0)
    {
        loadLevel();
        if (m_level+1 == m_levels.size())
        {
            std::cerr << "You did it you beat the levels";
        }
        else
        {
            m_level += 1;
            m_bricks = m_levels[m_level];
        }
    }
    if (m_pauseGame)
    {
        return;
    }
    m_timeSinceLastShot += dt;
    /*    vec2 paddleLeftCenter = m_paddleLeft + vec2{0, m_paddleDim.y*0.5};
    vec2 powerupCenter = m_powerup.pos + m_powerup.dims/2;
    if (squaredLength(paddleLeftCenter-powerupCenter) < m_b)
    {

    }*/
    performPowerupCollision(dt);


    handlePaddleInput(dt);

    bool preventBug = false;

    for (int i = 0; i < m_ball.size(); ++i)
    {
        m_ball[i].dt = dt;
    }

    for (int i = 0; i < m_ball.size(); ++i)
    {
        vec2 m_ballVel = m_ball[i].vel;
        vec2 m_ballPos = m_ball[i].pos;
        vec2 m_ballOffset = m_ball[i].ballOffset;
        bool m_launchBall = m_ball[i].launchball;
        float m_ballRadius =  m_ball[i].ballRadius;
        std::string m_balltype = m_ball[i].type;
        dt = m_ball[i].dt;

        unsigned iterations = 0;
        while (dt > 0 && iterations < 100 && m_launchBall == true)
        {
            auto brickCI = ballCollision(m_ballPos, m_ballVel, m_ballRadius, m_bricks, m_windowDim.x, m_windowDim.y);
            auto paddleCI = paddleCollision(m_ballRadius, m_paddleDim.y, m_ballVel, m_ballPos,
                                            vec2 {m_paddlePos.x - m_paddleDim.x/2, m_paddlePos.y - m_paddleDim.y/2},
                                            vec2 {m_paddlePos.x + m_paddleDim.x/2, m_paddlePos.y - m_paddleDim.y/2},
                                            vec2 {m_paddlePos.x - m_paddleDim.x/2, m_paddlePos.y},
                                            vec2 {m_paddlePos.x + m_paddleDim.x/2, m_paddlePos.y});
            auto ci = brickCI;
            bool collidedWithPaddle = false;
            if (brickCI.distance > paddleCI.distance)
            {
                ci = paddleCI;
                collidedWithPaddle = true;
            }
            auto fwdTime = ci.distance;
            if (fwdTime < 0)
            {
                throw std::logic_error("Negative time.");
            }
            if (fwdTime > dt || fwdTime < 0)
            {
                fwdTime = dt;
            }

    //        m_ballPos = {m_ballPos.x + m_ballVel.x * fwdTime, m_ballPos.y + m_ballVel.y * fwdTime};
            m_ballPos += fwdTime * m_ballVel;

            if (ci.distance <= dt && ci.distance >= 0)
            {
               // shape.setPosition(shape.getPosition().x + ballSpeed.x * fwdTime, shape.getPosition().y + ballSpeed.y * fwdTime);
                if (collidedWithPaddle && m_grabBall)
                {
                    m_launchBall = false;
                }
                else if (collidedWithPaddle)
                {
                    vec2 paddleCenter = m_paddlePos;
                    m_ballVel = normalise(m_ballPos-paddleCenter-vec2{0, m_paddleDim.y/2})*length(m_ballVel);
                    m_score -= 1;
                }
                else if (ci.brickNumber != -1)
                {
                    if (!m_passthrough && m_balltype != "laser")
                    {
                        m_ballVel = reflect(m_ballVel, ci.point);
                    }

                    m_score += m_bricks.at(ci.brickNumber).value;
                    if (m_bricks.at(ci.brickNumber).durability >= 0)
                    {
                        if (m_noDurability)
                        {
                            m_bricks.at(ci.brickNumber).durability = 0;
                        }
                        else
                        {
                            --m_bricks.at(ci.brickNumber).durability;
                        }
                    }
                    if (m_bricks.at(ci.brickNumber).durability == 0)
                    {
                        if (m_bricks.at(ci.brickNumber).texture == "gold")
                        {
                            m_noDurability = true;
                        }
                        m_bricks.erase(m_bricks.begin() + ci.brickNumber);
                        if (random(0, 10) >= 9)
                        {
                            m_powerup.push_back(generateRandomPowerup(i));
                        }
                    }

                    if (m_balltype == "laser" && !m_passthrough)
                    {
                        m_ball.erase(m_ball.begin() + i);
                        --i;
                        preventBug = true;
                        break;
                    }
                }
                else if (ci.brickNumber == -1)
                {
                    if (m_balltype == "laser")
                    {
                        m_ball.erase(m_ball.begin() + i);
                        --i;
                        preventBug = true;
                        break;
                    }
                    else
                    {
                        m_ballVel = reflect(m_ballVel, ci.point);
                    }
                }



                //  std::cout << "Dt er: " << dt << "\n";
            }
            if (!preventBug)
            {
            m_ball[i].vel = m_ballVel;
            m_ball[i].pos = m_ballPos;
            m_ball[i].ballOffset = m_ballOffset;
            m_ball[i].launchball = m_launchBall;
            m_ball[i].ballRadius = m_ballRadius;

            }
            else
            {
                preventBug = true;
            }
            dt -= fwdTime;
            ++iterations;
        }
    }
}

void Gamescreen::draw(sf::RenderWindow& window) const
{
    drawBricks(window);
    drawBall(window);
    drawPaddle(window);
    drawText(window);
    drawPowerup(window);
    if (m_enableGuns)
    {
        drawShooters(window);
    }
}

void Gamescreen::generateBricks()
{
    /*for (float x = m_windowDim.x/24; x < m_windowDim.x - m_windowDim.x/24; x += 2*m_windowDim.x/24)
    {
        for (float y = m_windowDim.y/32; y < m_windowDim.y - 7*m_windowDim.y/32; y += 2*m_windowDim.y/32)
        {
            Brick brick;
            vec2 position = vec2{x, y} + vec2{m_windowDim.x/32, 3};

            brick.push_back(position);
            brick.push_back(position + vec2{m_windowDim.x/24, 0});
            brick.push_back(position + vec2{m_windowDim.x/24, m_windowDim.y/32});
            brick.push_back(position + vec2{0, m_windowDim.y/32});
            brick.value = 100;
            brick.col = 0xffffff;
            brick.durability = 1;
            if (y < m_windowDim.y/3 && y > 0)
            {
                brick.texture = "diamond";
                std::cerr << "brick " << position.x/1920 << " " << position.y/1080 << " 24 32 1 100 " << "diamond\n";
            }
            else if (y > m_windowDim.y/3 && y < m_windowDim.y/2)
            {
                brick.texture = "gold";
                std::cerr << "brick " << position.x/1920 << " " << position.y/1080 << " 24 32 2 200 " << "gold\n";
            }
            else
            {
                brick.texture = "standard";
                std::cerr << "brick " << position.x/1920 << " " << position.y/1080 << " 24 32 1 100 " << "standard\n";
            }
            m_bricks.push_back(brick);
        }
    }*/
}

void Gamescreen::onDeath(float dt)
{
    for (int i = 0; i < m_ball.size(); ++i)
    {
        vec2 m_ballVel = m_ball[i].vel;
        vec2 m_ballPos = m_ball[i].pos;
        vec2 m_ballOffset = m_ball[i].ballOffset;
        bool m_launchBall = m_ball[i].launchball;
        float m_ballRadius =  m_ball[i].ballRadius;
        m_dead = true;
        m_launchBall = false;
        if (m_popTime <= 0.02)
        {
            m_popTime += dt;
        }
        else
        {
            m_popTime = 0;
            if (m_bricks.size() > 0)
            {
                m_score -= m_bricks[m_bricks.size()].value;
                m_bricks.erase(m_bricks.end()-1);
            }
        }
    }
}

void Gamescreen::drawPowerup(sf::RenderWindow& window) const
{
    for (int i = 0; i < m_powerup.size(); ++i)
    {
        sf::RectangleShape powerupDraw;
        sf::Sprite sprite;

        sprite.setPosition(m_powerup[i].pos.x, m_powerup[i].pos.y);
        sprite.setTexture(getTexture(m_powerup[i].texture));
        sprite.setScale(m_powerup[i].dims.x/48, m_powerup[i].dims.y/48);
        powerupDraw.setPosition(m_powerup[i].pos.x, m_powerup[i].pos.y);
        powerupDraw.setSize({m_powerup[i].dims.x, m_powerup[i].dims.y});
        powerupDraw.setFillColor(sf::Color::Cyan);
        window.draw(powerupDraw);
        window.draw(sprite);
    }
}

void Gamescreen::drawBall(sf::RenderWindow& window) const
{
    for (int i = 0; i < m_ball.size(); ++i)
    {
        vec2 m_ballPos = m_ball[i].pos;
        float m_ballRadius =  m_ball[i].ballRadius;

        sf::CircleShape ball(m_ballRadius);
        ball.setPosition(m_ballPos.x-m_ballRadius, m_ballPos.y-m_ballRadius);
        ball.setFillColor(sf::Color::Green);

        sf::Sprite sprite;



        sprite.setPosition(m_ballPos.x-m_ballRadius, m_ballPos.y-m_ballRadius);
        getTexture(m_ball[i].texture).setSmooth(true);
        sprite.setTexture(getTexture(m_ball[i].texture));
        if (m_ball[i].type == "laser")
        {
            sprite.setScale(m_ballRadius*2/20, m_ballRadius*2/20);
        }
        else
        {
            sprite.setScale(m_ballRadius*2/40, m_ballRadius*2/40);
        }
        /*sf::IntRect rect;
        rect.top = 0;
        rect.left = 0;
        rect.height = m_ballRadius*2/37.8;
        rect.width = rect.height;
        sprite.setTextureRect(rect);*/

        window.draw(sprite);
    }
}

void Gamescreen::drawBricks(sf::RenderWindow& window) const
{
    for (int i = 0; i < m_bricks.size(); ++i)
    {
        //First brick is the paddle.
        sf::RectangleShape brickDraw;
        brickDraw.setPosition({m_bricks[i][0].x, m_bricks[i][0].y});
        float width = length(m_bricks[i][1] - m_bricks[i][0]);
        float height = length(m_bricks[i][3] - m_bricks[i][0]);
        sf::Sprite sprite;

        sprite.setPosition(m_bricks[i][0].x, m_bricks[i][0].y);
        getTexture(m_bricks[i].texture).setSmooth(true);
        sprite.setTexture(getTexture(m_bricks[i].texture));
        sprite.setScale(width/80, height/34.5);
        //std::cerr << "Width: " << width << " Height: " << height << "\n";

        brickDraw.setSize({width, height});
//        window.draw(brickDraw);
        window.draw(sprite);
    }
}

void Gamescreen::drawPaddle(sf::RenderWindow& window) const
{
/*    Brick paddle;
    vec2 position = {m_paddlePos.x + m_paddleDim.x/2, m_paddlePos.y};
    paddle.push_back(position);
    paddle.push_back(position + vec2{m_paddleDim.x, 0});
    paddle.push_back(position + m_paddleDim);
    paddle.push_back(position + vec2{0, m_paddleDim.y});
    paddle.durability = -99;
    paddle.value = -1;*/
    auto halfWidth = m_paddleDim.x*0.5f;
    auto halfHeight = m_paddleDim.y*0.5f;
    sf::RectangleShape paddleDraw;
    paddleDraw.setPosition({m_paddlePos.x-halfWidth, m_paddlePos.y-halfHeight});
    paddleDraw.setSize({m_paddleDim.x, m_paddleDim.y});

    sf::Sprite middlePaddlePartSprite;
    middlePaddlePartSprite.setPosition(m_paddlePos.x-halfWidth, m_paddlePos.y-halfHeight);
    getTexture("paddleball").setSmooth(true);
    middlePaddlePartSprite.setTexture(getTexture("paddlebrick"));
    middlePaddlePartSprite.setScale(m_paddleDim.x/360, m_paddleDim.y/40);


    sf::CircleShape leftPaddlePart(halfHeight);
    leftPaddlePart.setPosition(m_paddlePos.x - halfWidth - halfHeight, m_paddlePos.y-halfHeight);
    leftPaddlePart.setFillColor(sf::Color::Green);

    sf::Sprite leftPaddlePartSprite;
    leftPaddlePartSprite.setPosition(m_paddlePos.x - halfWidth - halfHeight, m_paddlePos.y-halfHeight - halfHeight/42*2*1.1);
    getTexture("paddleball").setSmooth(true);
    leftPaddlePartSprite.setTexture(getTexture("paddleball"));
    leftPaddlePartSprite.setScale(halfHeight/42*2*1.1, halfHeight/42*2*1.1);


    sf::CircleShape rightPaddlePart(m_paddleDim.y/2);
    rightPaddlePart.setPosition(m_paddlePos.x + halfWidth - halfHeight, m_paddlePos.y-halfHeight);
    rightPaddlePart.setFillColor(sf::Color::Green);

    sf::Sprite rightPaddlePartSprite;
    rightPaddlePartSprite.setPosition(m_paddlePos.x + halfWidth - halfHeight*1.1, m_paddlePos.y-halfHeight - halfHeight/42*2*1.1);
    getTexture("paddleball").setSmooth(true);
    rightPaddlePartSprite.setTexture(getTexture("paddleball"));
    rightPaddlePartSprite.setScale(halfHeight/42*2*1.1, halfHeight/42*2*1.1);


    window.draw(leftPaddlePartSprite);
    window.draw(rightPaddlePartSprite);
    window.draw(middlePaddlePartSprite);
}

void Gamescreen::drawText(sf::RenderWindow& window) const
{
    sf::Font font;
    if(!font.loadFromFile("/usr/share/fonts/truetype/gentium/GentiumAlt-R.ttf"))
    {
        std::cerr << "No font found.\n";
    }

    std::stringstream scoreText;
    scoreText << "Score: " << m_score;
    sf::Text score(scoreText.str(), font);
    score.setCharacterSize(m_windowDim.x/64);
    score.setPosition(0+m_windowDim.x/64, 0);

    std::stringstream livesText;
    livesText << "Lives: " << m_lives;
    sf::Text lives(livesText.str(), font);
    lives.setCharacterSize(m_windowDim.x/64);
    lives.setPosition(m_windowDim.x-m_windowDim.x/18 , 0);

    window.draw(score);
    window.draw(lives);
}

void Gamescreen::drawShooters(sf::RenderWindow& window) const
{
    sf::Sprite spriteLeft;
    spriteLeft.setPosition(m_paddlePos.x-m_paddleDim.x/2-m_windowDim.y/110, m_paddlePos.y-m_paddleDim.y-m_windowDim.y/130);
    getTexture("lasershooter").setSmooth(true);
    spriteLeft.setTexture(getTexture("lasershooter"));
    spriteLeft.setScale(m_windowDim.x/1920, m_windowDim.y/1080);

    sf::Sprite spriteRight;
    spriteRight.setPosition(m_paddlePos.x+m_paddleDim.x/2-2*m_windowDim.y/100, m_paddlePos.y-m_paddleDim.y-m_windowDim.y/130);
    getTexture("lasershooter").setSmooth(true);
    spriteRight.setTexture(getTexture("lasershooter"));
    spriteRight.setScale(m_windowDim.x/1920, m_windowDim.y/1080);
    window.draw(spriteLeft);
    window.draw(spriteRight);
}

void Gamescreen::drawBackground(sf::RenderWindow& window, sf::Font font)
{
    sf::Text title("Breakout", font);
    title.setCharacterSize(m_windowDim.x/10);
    title.setPosition(m_windowDim.x/6, m_windowDim.y/100);

    sf::Sprite ballDuplicator;
    ballDuplicator.setPosition(m_windowDim.x*0.01, m_windowDim.y*0.3);
    getTexture("ballduplicator").setSmooth(true);
    ballDuplicator.setTexture(getTexture("ballduplicator"));
    ballDuplicator.setScale(m_windowDim.x*0.00075, m_windowDim.x*0.00075);

    sf::Text ballDuplicatorText("Duplicates\nball(s)", font);
    ballDuplicatorText.setCharacterSize(m_windowDim.x/64);
    ballDuplicatorText.setPosition(m_windowDim.x*0.05, m_windowDim.y*0.3);


    sf::Sprite ballOctuplicator;
    ballOctuplicator.setPosition(m_windowDim.x*0.01, m_windowDim.y*0.4);
    getTexture("octuplicate").setSmooth(true);
    ballOctuplicator.setTexture(getTexture("octuplicate"));
    ballOctuplicator.setScale(m_windowDim.x*0.00075, m_windowDim.x*0.00075);

    sf::Text ballOctuplicatorText("Octuplicates\nball(s)", font);
    ballOctuplicatorText.setCharacterSize(m_windowDim.x/64);
    ballOctuplicatorText.setPosition(m_windowDim.x*0.05, m_windowDim.y*0.4);


    sf::Sprite ballgrabber;
    ballgrabber.setPosition(m_windowDim.x*0.01, m_windowDim.y*0.5);
    getTexture("ballgrabber").setSmooth(true);
    ballgrabber.setTexture(getTexture("ballgrabber"));
    ballgrabber.setScale(m_windowDim.x*0.00075, m_windowDim.x*0.00075);

    sf::Text ballgrabberText("Grabs\nball(s)", font);
    ballgrabberText.setCharacterSize(m_windowDim.x/64);
    ballgrabberText.setPosition(m_windowDim.x*0.05, m_windowDim.y*0.5);

    sf::Sprite fastball;
    fastball.setPosition(m_windowDim.x*0.01, m_windowDim.y*0.6);
    getTexture("fastball").setSmooth(true);
    fastball.setTexture(getTexture("fastball"));
    fastball.setScale(m_windowDim.x*0.00075, m_windowDim.x*0.00075);

    sf::Text fastballText("Speed up\nball(s)", font);
    fastballText.setCharacterSize(m_windowDim.x/64);
    fastballText.setPosition(m_windowDim.x*0.05, m_windowDim.y*0.6);

    sf::Sprite slowball;
    slowball.setPosition(m_windowDim.x*0.01, m_windowDim.y*0.7);
    getTexture("slowball").setSmooth(true);
    slowball.setTexture(getTexture("slowball"));
    slowball.setScale(m_windowDim.x*0.00075, m_windowDim.x*0.00075);

    sf::Text slowballText("Slower\nball(s)", font);
    slowballText.setCharacterSize(m_windowDim.x/64);
    slowballText.setPosition(m_windowDim.x*0.05, m_windowDim.y*0.7);

    window.draw(title);
    window.draw(ballDuplicator);
    window.draw(ballDuplicatorText);
    window.draw(ballOctuplicator);
    window.draw(ballOctuplicatorText);
    window.draw(ballgrabber);
    window.draw(ballgrabberText);
    window.draw(fastball);
    window.draw(fastballText);
    window.draw(slowball);
    window.draw(slowballText);


    sf::Sprite fireball;
    fireball.setPosition(m_windowDim.x*0.15, m_windowDim.y*0.3);
    getTexture("fireball").setSmooth(true);
    fireball.setTexture(getTexture("fireball"));
    fireball.setScale(m_windowDim.x*0.00075, m_windowDim.x*0.00075);

    sf::Text fireballText("One hit\nbricks", font);
    fireballText.setCharacterSize(m_windowDim.x/64);
    fireballText.setPosition(m_windowDim.x*0.19, m_windowDim.y*0.3);


    sf::Sprite guns;
    guns.setPosition(m_windowDim.x*0.15, m_windowDim.y*0.4);
    getTexture("guns").setSmooth(true);
    guns.setTexture(getTexture("guns"));
    guns.setScale(m_windowDim.x*0.00075, m_windowDim.x*0.00075);

    sf::Text gunsText("You get\nguns!", font);
    gunsText.setCharacterSize(m_windowDim.x/64);
    gunsText.setPosition(m_windowDim.x*0.19, m_windowDim.y*0.4);


    sf::Sprite plusone;
    plusone.setPosition(m_windowDim.x*0.15, m_windowDim.y*0.5);
    getTexture("plusone").setSmooth(true);
    plusone.setTexture(getTexture("plusone"));
    plusone.setScale(m_windowDim.x*0.00075, m_windowDim.x*0.00075);

    sf::Text plusoneText("Plus one\nhp", font);
    plusoneText.setCharacterSize(m_windowDim.x/64);
    plusoneText.setPosition(m_windowDim.x*0.19, m_windowDim.y*0.5);

    sf::Sprite minusone;
    minusone.setPosition(m_windowDim.x*0.15, m_windowDim.y*0.6);
    getTexture("minusone").setSmooth(true);
    minusone.setTexture(getTexture("minusone"));
    minusone.setScale(m_windowDim.x*0.00075, m_windowDim.x*0.00075);

    sf::Text minusoneText("Minus one\nhp", font);
    minusoneText.setCharacterSize(m_windowDim.x/64);
    minusoneText.setPosition(m_windowDim.x*0.19, m_windowDim.y*0.6);

    sf::Sprite minusscore;
    minusscore.setPosition(m_windowDim.x*0.15, m_windowDim.y*0.7);
    getTexture("minusscore").setSmooth(true);
    minusscore.setTexture(getTexture("minusscore"));
    minusscore.setScale(m_windowDim.x*0.00075, m_windowDim.x*0.00075);

    sf::Text minusscoreText("Minus 10000\nscore", font);
    minusscoreText.setCharacterSize(m_windowDim.x/64);
    minusscoreText.setPosition(m_windowDim.x*0.19, m_windowDim.y*0.7);

    window.draw(fireball);
    window.draw(fireballText);
    window.draw(guns);
    window.draw(gunsText);
    window.draw(plusone);
    window.draw(plusoneText);
    window.draw(minusone);
    window.draw(minusoneText);
    window.draw(minusscore);
    window.draw(minusscoreText);



    sf::Sprite paddlelarger;
    paddlelarger.setPosition(m_windowDim.x*0.29, m_windowDim.y*0.3);
    getTexture("paddlelarger").setSmooth(true);
    paddlelarger.setTexture(getTexture("paddlelarger"));
    paddlelarger.setScale(m_windowDim.x*0.00075, m_windowDim.x*0.00075);

    sf::Text paddlelargerText("Makes the\npaddle larger", font);
    paddlelargerText.setCharacterSize(m_windowDim.x/64);
    paddlelargerText.setPosition(m_windowDim.x*0.33, m_windowDim.y*0.3);


    sf::Sprite paddlesmaller;
    paddlesmaller.setPosition(m_windowDim.x*0.29, m_windowDim.y*0.4);
    getTexture("paddlesmaller").setSmooth(true);
    paddlesmaller.setTexture(getTexture("paddlesmaller"));
    paddlesmaller.setScale(m_windowDim.x*0.00075, m_windowDim.x*0.00075);

    sf::Text paddlesmallerText("Makes the\npaddle smaller", font);
    paddlesmallerText.setCharacterSize(m_windowDim.x/64);
    paddlesmallerText.setPosition(m_windowDim.x*0.33, m_windowDim.y*0.4);


    sf::Sprite thrublock;
    thrublock.setPosition(m_windowDim.x*0.29, m_windowDim.y*0.5);
    getTexture("thrublock").setSmooth(true);
    thrublock.setTexture(getTexture("thrublock"));
    thrublock.setScale(m_windowDim.x*0.00075, m_windowDim.x*0.00075);

    sf::Text thrublockText("Goes through\nbricks", font);
    thrublockText.setCharacterSize(m_windowDim.x/64);
    thrublockText.setPosition(m_windowDim.x*0.33, m_windowDim.y*0.5);

    window.draw(paddlelarger);
    window.draw(paddlelargerText);
    window.draw(paddlesmaller);
    window.draw(paddlesmallerText);
    window.draw(thrublock);
    window.draw(thrublockText);
}

std::string Gamescreen::drawMainMenu(sf::RenderWindow& window, bool isClicked)
{
    sf::Font font;
    if(!font.loadFromFile("/usr/share/fonts/truetype/gentium/GentiumAlt-R.ttf"))
    {
        std::cerr << "No font found.\n";
    }
    drawBackground(window, font);

    sf::Text newGame("New game", font);
    newGame.setCharacterSize(m_windowDim.x/48);
    newGame.setPosition(m_windowDim.x-m_windowDim.x/6, m_windowDim.y*0.4);
    sf::FloatRect boundingBox = newGame.getGlobalBounds();
    vec2 mousePos = {sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y};
    if (boundingBox.contains({mousePos.x, mousePos.y}))
    {
        newGame.setColor(sf::Color(200, 200, 200, 128));
        if (isClicked)
        {
            return "game";
        }
    }

    sf::Text editor("Editor", font);
    editor.setCharacterSize(m_windowDim.x/48);
    editor.setPosition(m_windowDim.x-m_windowDim.x/6, m_windowDim.y*0.5);
    sf::FloatRect editorBox = editor.getGlobalBounds();
    if (editorBox.contains({mousePos.x, mousePos.y}))
    {
        editor.setColor(sf::Color(200, 200, 200, 128));
        if (isClicked)
        {
            return "editor";
        }
    }

    sf::Text exitGame("Exit game", font);
    exitGame.setCharacterSize(m_windowDim.x/48);
    exitGame.setPosition(m_windowDim.x-m_windowDim.x/6, m_windowDim.y*0.6);
    sf::FloatRect exitGameBox = exitGame.getGlobalBounds();
    if (exitGameBox.contains({mousePos.x, mousePos.y}))
    {
        exitGame.setColor(sf::Color(200, 200, 200, 128));
        if (isClicked)
        {
            window.close();
        }
    }

    window.draw(newGame);
    window.draw(editor);
    window.draw(exitGame);
    return "empty";
}

std::string Gamescreen::mainMenu(sf::RenderWindow& window, bool isClicked)
{
    std::string type = drawMainMenu(window, isClicked);
    return type;
}

void Gamescreen::writeLevel(std::ostream& config)
{
    config << "\n" << "Level " << m_currLevel + 2 << "\n";
    for (int i = 0; i < m_editorBricks.size(); ++i)
    {
        config << "brick " << m_editorBricks[i][0].x/1920 << " " << m_editorBricks[i][0].y/1080
               << " 24 32 " << m_editorBricks[i].durability << " " << m_editorBricks[i].value
               << " " << m_editorBricks[i].texture << "\n";
    }
    config << "endoflevel\n";
    config << std::flush;
}

void Gamescreen::eraseEditorBricks(sf::RenderWindow& window)
{
    for (int i = 0; i < m_editorBricks.size(); ++i)
    {
        if (vec2{m_editorBricks[i][0].x, m_editorBricks[i][0].y} == m_editorBrickPos)
        {
            m_editorBricks.erase(m_editorBricks.begin()+i);
        }
    }
}

void Gamescreen::drawEditorBricks(sf::RenderWindow& window)
{

    for (int i = 0; i < m_editorBricks.size(); ++i)
    {
        //First brick is the paddle.
        sf::RectangleShape brickDraw;
        brickDraw.setPosition({m_editorBricks[i][0].x, m_editorBricks[i][0].y});
        float width = length(m_editorBricks[i][1] - m_editorBricks[i][0]);
        float height = length(m_editorBricks[i][3] - m_editorBricks[i][0]);
        sf::Sprite sprite;

        sprite.setPosition(m_editorBricks[i][0].x, m_editorBricks[i][0].y);
        getTexture(m_editorBricks[i].texture).setSmooth(true);
        sprite.setTexture(getTexture(m_editorBricks[i].texture));
        sprite.setScale(width/80, height/34);
        //std::cerr << "Width: " << width << " Height: " << height << "\n";

        brickDraw.setSize({width, height});
//        window.draw(brickDraw);
        window.draw(sprite);
    }
}

void Gamescreen::setEditorBricks(sf::RenderWindow& window)
{
    for (int i = 0; i < m_editorBricks.size(); ++i)
    {
        if (vec2{m_editorBricks[i][0].x, m_editorBricks[i][0].y} == m_editorBrickPos)
        {
            m_editorBricks.erase(m_editorBricks.begin()+i);
        }
    }
    Brick brick;
    brick.points.push_back(m_editorBrickPos);
    brick.points.push_back(m_editorBrickPos + vec2{m_windowDim.x/24, 0});
    brick.points.push_back(m_editorBrickPos + vec2{m_windowDim.x/24, m_windowDim.y/32});
    brick.points.push_back(m_editorBrickPos + vec2{0, m_windowDim.y/32});
    if (m_texture == "gold")
    {
        brick.durability = 10;
        brick.value = 500;
    }
    if (m_texture == "poop")
    {
        brick.durability = 2;
        brick.value = 10;
    }
    else
    {
        brick.durability = 1;
        brick.value = 100;
    }
    brick.texture = m_texture;
    m_editorBricks.push_back(brick);
}

void Gamescreen::createBricks(sf::RenderWindow& window)
{
        sf::Sprite sprite;
        sprite.setPosition({m_editorBrickPos.x, m_editorBrickPos.y});
        getTexture(m_texture).setSmooth(true);
        sprite.setTexture(getTexture(m_texture));
        float width = m_windowDim.x/24;
        float height = m_windowDim.y/32;
        sprite.setScale(width/80, height/34);
        sprite.setColor(sf::Color(200, 200, 200, 128));

        window.draw(sprite);
}

void Gamescreen::handleEditorInput(sf::RenderWindow& window, std::ostream& config)
{
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q)
        || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
    {
        window.close();
    }
    sf::Event event;
    while (window.pollEvent(event))
    {
        if (event.type == sf::Event::Closed)
        {
            window.close();
        }
        if ((event.type == sf::Event::KeyPressed)
            && (event.key.code == sf::Keyboard::Up)
            && (m_editorBrickPos - vec2{0, m_windowDim.y/32}).y >= 0)
        {
            m_editorBrickPos -= vec2{0, m_windowDim.y/32};
        }
        if ((event.type == sf::Event::KeyPressed)
            && (event.key.code == sf::Keyboard::Down)
            && (m_editorBrickPos + vec2{0, m_windowDim.y/32}).y <= m_windowDim.y-m_windowDim.y/3)
        {
            m_editorBrickPos += vec2{0, m_windowDim.y/32};
        }
        if ((event.type == sf::Event::KeyPressed)
            && (event.key.code == sf::Keyboard::Left)
            && (m_editorBrickPos - vec2{m_windowDim.x/24, 0}).x >= 0)
        {
            m_editorBrickPos -= vec2{m_windowDim.x/24, 0};
        }
        if ((event.type == sf::Event::KeyPressed)
            && (event.key.code == sf::Keyboard::Right)
            && (m_editorBrickPos + vec2{2*m_windowDim.x/24, 0}).x <= m_windowDim.x)
        {
            m_editorBrickPos += vec2{m_windowDim.x/24, 0};
        }
        if ((event.type == sf::Event::KeyPressed)
            && (event.key.code == sf::Keyboard::Space))
        {
            setEditorBricks(window);
        }
        if ((event.type == sf::Event::KeyPressed)
            && (event.key.code == sf::Keyboard::Delete))
        {
            eraseEditorBricks(window);
        }
        if ((event.type == sf::Event::KeyPressed)
            && (event.key.code == sf::Keyboard::Num1))
        {
            m_texture = "standard";
        }
        if ((event.type == sf::Event::KeyPressed)
            && (event.key.code == sf::Keyboard::Num2))
        {
            m_texture = "gold";
        }
        if ((event.type == sf::Event::KeyPressed)
            && (event.key.code == sf::Keyboard::Num3))
        {
            m_texture = "diamond";
        }
        if ((event.type == sf::Event::KeyPressed)
            && (event.key.code == sf::Keyboard::Num4))
        {
            m_texture = "grass";
        }
        if ((event.type == sf::Event::KeyPressed)
            && (event.key.code == sf::Keyboard::Num5))
        {
            m_texture = "poop";
        }
        if ((event.type == sf::Event::KeyPressed)
            && (event.key.code == sf::Keyboard::Num6))
        {
            m_texture = "white";
        }
        if ((event.type == sf::Event::KeyPressed)
            && (event.key.code == sf::Keyboard::S))
        {
            writeLevel(config);
            window.close();
        }
    }
}

void Gamescreen::editorMode(sf::RenderWindow& window, std::ostream& config)
{
    handleEditorInput(window, config);
    drawEditorBricks(window);
    createBricks(window);
}
