#ifndef GAMESCREEN_HPP_INCLUDED
#define GAMESCREEN_HPP_INCLUDED

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
//Kraken has been used with permission from Thor G. Nielsen at https://gitlab.com/ThorNielsen/kraken
#include <kraken/math/vector.hpp>
#include <vector>
#include <kraken/math/matrix.hpp>
#include <random>
#include <functional>
#include <map>
#include <iostream>
#include <fstream>

using namespace kraken::math;

double random(double low, double high, unsigned seed = 0xdeadbeef);

struct Brick
{
    std::vector<vec2> points;
    unsigned col;
    int durability;
    int value;
    std::string texture;

    vec2& operator[](size_t i)
    {
        return points[i];
    }
    const vec2& operator[](size_t i) const
    {
        return points[i];
    }
    size_t size() const
    {
        return points.size();
    }
    void push_back(vec2 p)
    {
        points.push_back(p);
    }
};

struct Ball
{
    vec2 vel;
    vec2 pos;
    vec2 ballOffset;
    bool launchball;
    float ballRadius;
    float dt;
    std::string type;
    std::string texture;
};

/*
NewBrick brick;
brick.push_back({0, 0});
brick.push_back({0, 0});
brick.col = 0x0f0f0f;
brick.durability = 1;
brick.value = 100;
*/

class Gamescreen
{
public:
    Gamescreen(sf::Window& window, std::istream& config);
    void handleInput(sf::Window& window);
    void update(float dt);
    void draw(sf::RenderWindow& window) const;
    void reset(sf::Window& window, std::istream& config);
    void duplicateBall();
    void octuplicateBall();
    void setPaddleWidth(float width);
    void enableGuns();
    void shootWithGuns();
    void enableBallGrabber();
    void setBallSpeed(float speed);
    void enablePassthrough();
    void enableFireball();
    void modifyLife(int change);
    void modifyScore(int change);
    float getPaddleWidth();
    float getBallSpeed();
    vec2 getScreenSize();

    std::string mainMenu(sf::RenderWindow& window, bool isClicked);
    void drawBackground(sf::RenderWindow& window, sf::Font font);

    //editor functions
    void writeLevel(std::ostream& config);
    void eraseEditorBricks(sf::RenderWindow& window);
    void drawEditorBricks(sf::RenderWindow& window);
    void setEditorBricks(sf::RenderWindow& window);
    void createBricks(sf::RenderWindow& window);
    void handleEditorInput(sf::RenderWindow& window, std::ostream& config);
    void editorMode(sf::RenderWindow& window, std::ostream& config);


private:
    using Level = std::vector<Brick>;

    struct Powerup
    {
        vec2 pos;
        vec2 vel;
        vec2 dims;
        std::function<void(Gamescreen&)> action;
        std::string texture;
    };

    void readLevel(std::istream& config);
    void loadLevel();
    void generateBricks();
    void onDeath(float dt);
    void performPowerupCollision(float dt);
    Powerup generateRandomPowerup(int i);
    void handlePaddleInput(float dt);
    sf::Texture& getTexture(std::string name) const;

    void drawPowerup(sf::RenderWindow& window) const;
    void drawBall(sf::RenderWindow& window) const;
    void drawBricks(sf::RenderWindow& window) const;
    void drawPaddle(sf::RenderWindow& window) const;
    void drawText(sf::RenderWindow& window) const;
    void drawShooters(sf::RenderWindow& window) const;
    std::string drawMainMenu(sf::RenderWindow& window, bool isClicked);

    Level m_bricks;
    std::vector<Level> m_levels;
    std::vector<Powerup> m_powerup;
    std::vector<Ball> m_ball;
    mutable std::map<std::string, sf::Texture> m_textures;
    vec2 m_windowDim;
    vec2 m_paddleDim;
    vec2 m_paddlePos;
    float m_paddleVel;
    float m_mousePos;
    float m_popTime;
    float m_timeSinceLastShot;
    int m_score;
    int m_lives;
    int m_level;
    bool m_paddleLeft;
    bool m_paddleRight;
    bool m_ballLeft;
    bool m_ballRight;
    bool m_dead;
    bool m_passthrough;
    bool m_grabBall;
    bool m_enableGuns;
    bool m_pauseGame;
    bool m_wait;
    bool m_noDurability;
    bool m_enableCheats;
    bool m_allowSkipLevel;
    size_t m_currLevel;

    //Editor functions
    Level m_editorBricks;
    int m_currElem;
    vec2 m_editorBrickPos;
    std::string m_texture;
};

#endif // GAMESCREEN_HPP_INCLUDED
